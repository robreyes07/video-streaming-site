<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="chrome=1"/>
    <title>Video Streaming Test</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>
    <meta name="theme-color" content="#ffffff"/>
    <meta name="url" content="{{ env('APP_URL') }}"/>
    <meta name="status" content="{{ App::environment() }}"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <!-- Stylesheets, Fonts and icons -->
    <link href='{{ mix('/css/main.css') }}' rel="stylesheet"/>

</head>
<body>

<div id="app"></div>

<!--   Scripts   -->
<script src="{{ mix('/js/main.js') }}" type="text/javascript"></script>
</body>
</html>