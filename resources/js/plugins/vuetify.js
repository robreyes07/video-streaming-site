import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import '@fortawesome/fontawesome-free/css/all.css'

Vue.use(Vuetify)

const opts = {
  theme: {
    dark: true,
    
    themes: {
      light: {
        primary: '#3f51b5',
        secondary: '#673ab7',
        accent: '#2196f3',
        error: '#f44336',
        warning: '#ffc107',
        info: '#00bcd4',
        success: '#4caf50'
      },
      dark: {
        primary: '#3f51b5',
        secondary: '#673ab7',
        accent: '#2196f3',
        error: '#f44336',
        warning: '#ffc107',
        info: '#00bcd4',
        success: '#4caf50'
      }
    },
  },

  icons: {
    iconfont: 'fa',
  }
}

export default new Vuetify(opts)
