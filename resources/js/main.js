import './bootstrap'
import vuetify from './plugins/vuetify'
import Vue from 'vue'
import App from './App'

Vue.component('header-component', require('./components/HeaderComponent.vue'));
Vue.component('login-component', require('./components/LoginButtonComponent.vue'));
Vue.component('register-button', require('./components/RegisterButtonComponent.vue'));
Vue.component('remember-password', require('./components/RememberPasswordComponent.vue'));
Vue.component('reset-password', require('./components/ResetPasswordComponent.vue'));
Vue.component('snackbar', require('./components/SnackBarComponent.vue'));
Vue.component('gravatar', require('./components/GravatarComponent.vue'));

import router from './router'
import store from './store'
import * as mutations from './store/mutation-types'

import withSnackbar from './components/mixins/withSnackbar'

if (window.user) {
  store.commit(mutations.USER,  user)
  store.commit(mutations.LOGGED, true)
}

new Vue({
  router,
  vuetify,
  store,
  mixins: [ withSnackbar ],
  render: h => h(App)
}).$mount('#app')

