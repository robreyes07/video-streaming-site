import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: require('./views/Home'),
    meta: {
      title: 'Welcome to video stream test'
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: require('./views/Login'),
    meta: {
      title: 'Welcome to video stream test - login'
    }
  }
  
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

export default router
